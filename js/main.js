menuMobile();
abreModalCompra();


function menuMobile() {

    var x = document.querySelector('.mobile-btn');

    x.onclick = function () {
        this.classList.toggle("active");
        document.querySelector('.menu-mb').classList.toggle("active");
    }
}


function abreModalCompra() {
         
    var modal = document.querySelector('.cart-modal');
    var btnFecha = document.querySelector('.btn-fechar');
    var btnAbre = document.querySelector('.produto-submit');

    btnAbre.onclick = function() {
        modal.style.display = "block";
    }

    btnFecha.onclick = function () {
        modal.style.display = "none"; 
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}